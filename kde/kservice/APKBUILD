# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kservice
pkgver=5.52.0
pkgrel=0
pkgdesc="Advanced plugin and service introspection"
arch="all"
url="https://community.kde.org/Frameworks"
license="LGPL-2.1"
depends=""
depends_dev="kcrash-dev kconfig-dev kdbusaddons-dev ki18n-dev kcoreaddons-dev"
makedepends="$depends_dev extra-cmake-modules kdoctools-dev doxygen qt5-qttools-dev flex-dev bison"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/${pkgname}-${pkgver}.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
options="!check" # Fails due to test suites not building

prepare() {
	mkdir "$builddir"/build
}

build() {
	cd "$builddir"/build
	cmake \
		-DCMAKE_BUILD_TYPE=Release \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DKDE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON \
		-DBUILD_TESTING=OFF ..
	make
}

check() {
	cd "$builddir"/build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"/build
	make DESTDIR="${pkgdir}" install
}

sha512sums="7c3ba6dab342c6c86b849d88dba0f160871a619fb7f3c3abaa9ccd4ead3687c909ea15ccf3accb5304e9d1f702d4a416b971f0b88c19f07604c08881288e4e35  kservice-5.52.0.tar.xz"
